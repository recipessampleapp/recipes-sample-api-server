import React, { Component, getGlobal, setGlobal } from 'reactn';
import {
    Link,
    Redirect
} from "react-router-dom";

class Header extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            redirectLogin: false
        }
        this.logout = this.logout.bind(this)
    }
    logout() {
        localStorage.clear()
        setGlobal({
            accessToken: null,
            userIsLoggedIn: null
        })    
        setTimeout(() => {
            window.location.href = "/login";
        }, 500)        
    }
    render() {
        if (this.state.redirectLogin) {
            return (
                <Redirect to={"/login"} />
            )
        }

        return (
            
                getGlobal().userIsLoggedIn &&
                    <section id="Header" className="flex flex-column w-100 bb b--black-05">
                        <div className="flex flex-row mw8 w-100 center justify-between">
                            <div className="flex flex-row w-50 justify-start items-center">
                                <span className="f4 fw6 black ph3 pv3">Recipes</span>
                            </div>
                            <div className="flex flex-row w-50 justify-end">
                                <div className="flex flex-column">
                                    <Link className="ph4 pv3 bl b--black-05 black-60" to="/">View Recipes</Link>
                                </div>
                                <div className="flex flex-column">
                                    <Link className="ph4 pv3 bl b--black-05 black-60" to="/add">Add Recipe</Link>
                                </div>
                                <div className="flex flex-column">
                                    <div onClick={this.logout} className="pointer ph4 pv3 bl b--black-05 black-60">Logout</div>
                                </div>
                            </div>
                        </div>
                    </section>

            

        )
    }
}
export default Header