import React, { Component } from 'reactn';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    Redirect
} from "react-router-dom";

import methods from '../../utils';

import Wrapper from '../../components/Wrapper';
import List from '../../components/list';
import PageTitle from '../../components/PageTitle';
import Filter from '../../components/filter';
import Search from '../../components/search';

import { notification } from 'antd';

class Home extends React.Component {
    constructor() {
        super()
        this.state = {
            recipes: [],
            cuisines: [],
            tags: [],
            ready: false,
            buttonLoading: false,
            redirectHome: false
        }
        this.setFilter = this.setFilter.bind(this)
        this.setCuisines = this.setCuisines.bind(this)
        this.importDemoData = this.importDemoData.bind(this)
        this.searchByIngredients = this.searchByIngredients.bind(this)
        this.initialiseSearch = this.initialiseSearch.bind(this);
        this.initialiseSearchTags = this.initialiseSearchTags.bind(this);
        this.openNotification = this.openNotification.bind(this);
        this.reload = this.reload.bind(this);
        this.init = this.init.bind(this)
    }
    openNotification = () => {
        notification.open({
            message: 'Success',
            description:
                'The sample data has been imported.',
            onClick: () => {
            }
        })
    }
    async initialiseSearchTags() {
        let recipes = this.state.recipes;
        let tags = await methods.initialiseSearchTags(recipes)
        this.setState({
            tags: tags
        })
    }
    async searchByIngredients(tags) {
        this.initialiseSearch();
        let recipes = await methods.fetchRecipes()
        this.setState({
            recipes: recipes,
            ready: true
        })
        if(tags.length > 0) {
        let res = await methods.fetchRecipesByIngredients({ tags, recipes })
        console.log('[[ Search Results ]]',res);
        this.setState({recipes: res})
        }        
    }
    initialiseSearch() {
        let recipes = this.state.recipes;       
    }
    async setFilter(value) {
        this.setState({
            ready: false
        })
        if(value === "none"){
            let recipes = await methods.fetchRecipes()
            this.setState({
                recipes: recipes,
                ready: true
            })
            this.setCuisines()
        }
        else {
            let cuisine = value;
            let recipes = await methods.fetchRecipesByCuisine({ cuisine });
            this.setState({
                recipes: recipes,
                ready: true
            })
        }
    }
    setCuisines() {
        let cuisines = [];
        let recipes = this.state.recipes;
        for(let item of recipes){
            if(!cuisines.includes(item.cuisine)) {
                cuisines.push(item.cuisine)
            }
        }
        this.setState({cuisines: cuisines})
    }
    async importDemoData() {
        this.setState({
            buttonLoading: true
        })
        let res = await methods.importDemoData()
        console.log('[[ Import Demo Data ]]',res);
        if(res.length === res.res.length) {
            this.openNotification()
            setTimeout(() => {
                this.init()
            },1000)
        }
    }
    reload() {
        this.setState({
            redirectHome: true
        })
        this.setState({
            redirectHome: false
        })
    }
    async init() {
        let recipes = await methods.fetchRecipes()
        this.setState({
            recipes: recipes,
            ready: true
        })
        this.setCuisines()
        this.initialiseSearchTags()
    }
    async componentDidMount() {
        this.init()
    }
    render() {   
        return(
            <Wrapper>
            <section id="Home" className="flex flex-column w-100">

                <div className="flex flex-column flex-row-ns">
                    <PageTitle title={"Recipes List"} subtitle={"Browse a selection of recipes below"} />
                    <div className="flex flex-column w-100 w-50-ns justify-center pb4 pb0-ns">
                        <Filter cuisines={this.state.cuisines} setFilter={this.setFilter} />   
                        <Search search={this.searchByIngredients} tags={this.state.tags} /> 
                    </div>           
                </div>    

                <List buttonLoading={this.state.buttonLoading} ready={this.state.ready} recipes={this.state.recipes} importDemoData={this.importDemoData}/>

            </section>
            </Wrapper>
        )
    }
}

export default Home